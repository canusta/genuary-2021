import './style.css'
import * as THREE from 'three'
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js'
import * as dat from 'dat.gui'

let mouse = new THREE.Vector2()
function onDocumentMouseMove(event) {
    event.preventDefault();
    mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
    mouse.y = -(event.clientY / window.innerHeight) * 2 + 1;
}
document.addEventListener('mousemove', onDocumentMouseMove, false);

// Debug
const gui = new dat.GUI()

// Canvas
const canvas = document.querySelector('canvas.webgl')

// Scene
const scene = new THREE.Scene()

// Objects
const geometry = new THREE.TorusGeometry( .7, .2, 8, 8 );

// Materials

const material = new THREE.MeshPhongMaterial({flatShading: true})
material.color = new THREE.Color(0x0000ff)

let objects = []

for (let i = 0; i < 10000; i++) {
    const sphere = new THREE.Mesh(geometry,material)
    sphere.position.x = 12 - ( Math.random() *24)
    sphere.position.y = 8 - ( Math.random() *16)
    sphere.position.z = 0 - ( Math.random() *4)
    scene.add(sphere)
    objects.push(sphere)
}

// Lights

const pointLight = new THREE.DirectionalLight( 0xffffff, 1, 100 );

// const light = new THREE.DirectionalLight( 0xffffff, 1, 100 );
pointLight.position.x = 2
pointLight.position.y = 30
pointLight.position.z = 4
scene.add(pointLight)

/**
 * Sizes
 */
const sizes = {
    width: window.innerWidth,
    height: window.innerHeight
}

window.addEventListener('resize', () =>
{
    // Update sizes
    sizes.width = window.innerWidth
    sizes.height = window.innerHeight

    // Update camera
    camera.aspect = sizes.width / sizes.height
    camera.updateProjectionMatrix()

    // Update renderer
    renderer.setSize(sizes.width, sizes.height)
    renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))
})

/**
 * Camera
 */
// Base camera
const camera = new THREE.PerspectiveCamera(75, sizes.width / sizes.height, 0.1, 100)
camera.position.x = 0
camera.position.y = 0
camera.position.z = 10
scene.add(camera)

// Controls
const controls = new OrbitControls(camera, canvas)
controls.enableDamping = true

/**
 * Renderer
 */
const renderer = new THREE.WebGLRenderer({
    canvas: canvas
})
renderer.setSize(sizes.width, sizes.height)
renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))

/**
 * Animate
 */

const clock = new THREE.Clock()

const tick = () =>
{

    const elapsedTime = clock.getElapsedTime()

    // Update objects
    // sphere.rotation.y = .5 * elapsedTime
    for (let i = 0; i < objects.length; i++) {
        const e = objects[i];
        e.rotation.y = .2 * elapsedTime
        e.position.z = i/Math.sin(mouse.x)/100
        e.position.y = i/Math.sin(mouse.y)/100
    }

    // Update Orbital Controls
    controls.update()

    // Render
    renderer.render(scene, camera)

    // Call tick again on the next frame
    window.requestAnimationFrame(tick)
}

tick()